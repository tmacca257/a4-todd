import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class TestBug2CannotReachBetLimit {
	//initilise variables
	Player player1;
	Player player2;
	
	@Before
	public void setup() throws Exception 
	{
		
	}
	
	
	@After
	public void tearDown() throws Exception
	{
	}
	
	
	@Test
	public void testBug2CannotReachBetLimit()
	{
		//arrange
		player1 = new Player("Fred", 50); //balance set to 50
		player2 = new Player("Jack", 5); //balance set to 5
		assertTrue(player1.balanceExceedsLimitBy(5));
		assertTrue(player2.balanceExceedsLimitBy(5));
	}
}