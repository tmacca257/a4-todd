import static org.junit.Assert.*;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.List;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class TestBug3OddsNotCorrect {
	
	BufferedReader console = new BufferedReader(new InputStreamReader(System.in));
	
	Dice d1;
    Dice d2;
    Dice d3;

    Player player = new Player("Fred", 100);
    Game game;
    List<DiceValue> cdv;

    int totalWins = 0;
    int totalLosses = 0;
    
	int winCount;
	int loseCount;
	
	@Before
	public void setup() throws Exception 
	{
	}
	
	
	@After
	public void tearDown() throws Exception
	{
	}

	@Test
	public void testOddsNotCorrect() {
		
		//arrange
        winCount = 0;
        loseCount = 0;
            
        //execute
        for (int i = 0; i < 100; i++)
        {
        	String name = "Fred";
        	int balance = 100;
        	int limit = 0;
            player = new Player(name, balance);
            player.setLimit(limit);
            int bet = 5;
            
            while (player.balanceExceedsLimitBy(bet) && player.getBalance() < 200)
            {
            	d1 = new Dice();
                d2 = new Dice();
                d3 = new Dice();
                game = new Game(d1, d2, d3);
                cdv = game.getDiceValues();
            	DiceValue pick = DiceValue.getRandom();
                        	
            	int winnings = game.playRound(player, pick, bet);
                cdv = game.getDiceValues();
                
                if (winnings > 0) {
                	winCount++; 
                }
                else {
                	loseCount++;
                }
                
            } //while
        } //for
            
        float winlose = (float)winCount/(winCount+loseCount);
        System.out.println("win+lose ratio is" +winlose);
        
        //assert
        assertTrue(winlose >= 0.41 && winlose <= 0.43);
		}
}