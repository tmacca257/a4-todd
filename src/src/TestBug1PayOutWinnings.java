import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class TestBug1PayOutWinnings {
	
	//initilise variables
	Player player;
	Game game;
	int winnings;
	Dice d1;
	Dice d2;
	Dice d3;

	@Before
	public void setup() throws Exception 
	{
		d1 = new Dice();
        d2 = new Dice();
        d3 = new Dice();
		
        player = new Player("Fred", 100);
		game = new Game(d1, d2, d3);
		winnings = 0;
	}
	
	
	@After
	public void tearDown() throws Exception
	{
	}
	
	
	@Test
	public void testWinningsEqualsMatchesTimesBet() 
	{
		//test bug1
		//arrange
		int bet = 5;
		DiceValue pick = DiceValue.ANCHOR; //
		
		int matches = 0;
		if (d1.getValue().equals(pick)) {matches++;}
		if (d2.getValue().equals(pick)) {matches++;}
		if (d3.getValue().equals(pick)) {matches++;}
		
		
		//execute
		winnings = game.playRound(player, pick, bet);
		
		//assert
		assertTrue(winnings > matches*bet || matches == 0);
		System.out.println("Winnings = "+winnings+ " & matches = "+matches);
	}
}